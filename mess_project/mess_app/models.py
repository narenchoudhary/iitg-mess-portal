from __future__ import unicode_literals, absolute_import

from django.contrib.auth.models import AbstractUser
from django.core.validators import MaxValueValidator
from django.db import models
from django.utils import timezone
from django.utils.encoding import python_2_unicode_compatible
from django.utils.translation import ugettext_lazy as _

from .constants import *


@python_2_unicode_compatible
class UserProfile(AbstractUser):
    """
    UserProfile model class
    """
    user_type = models.CharField(max_length=30, choices=USER_TYPE)
    hostel = models.CharField(max_length=40, choices=HOSTEL, default='Other')

    def __str__(self):
        return self.username


@python_2_unicode_compatible
class Meal(models.Model):
    """
    Meal model class.
    """
    menu_items = models.TextField()
    extra_items = models.TextField(
        blank=True, help_text=_('Provide all necessary details.'))
    special_dinner = models.BooleanField(default=False)
    price = models.DecimalField(max_digits=5, decimal_places=2)
    food_type = models.CharField(max_length=20, choices=FOOD_TYPE)
    creation_datetime = models.DateTimeField(blank=True)
    last_updated = models.DateTimeField(blank=True)

    def save(self, *args, **kwargs):
        if not self.id:
            self.creation_datetime = timezone.now()
        self.last_updated = timezone.now()
        super(Meal, self).save(*args, **kwargs)

    def __str__(self):
        return str(self.food_type)


@python_2_unicode_compatible
class Day(models.Model):
    """
    Day model class.

    This model class contains information about all meals of a weekday.
    The meals can be breakfast, lunch and dinner.
    """
    day = models.PositiveIntegerField(validators=[MaxValueValidator(7), ])
    breakfast = models.ForeignKey(Meal, related_name='breakfast', null=True)
    lunch = models.ForeignKey(Meal, related_name='lunch', null=True)
    dinner = models.ForeignKey(Meal, related_name='dinner', null=True)
    creation_datetime = models.DateTimeField(blank=True)
    last_updated = models.DateTimeField(blank=True)

    def save(self, *args, **kwargs):
        if not self.id:
            self.creation_datetime = timezone.now()
        self.last_updated = timezone.now()
        super(Day, self).save(*args, **kwargs)

    def __str__(self):
        return str(self.day)


@python_2_unicode_compatible
class Mess(models.Model):
    """
    Mess menu model object.
    """
    hostel = models.CharField(max_length=50, choices=HOSTEL, unique=True)
    day_1 = models.ForeignKey(Day, related_name='day_1', null=True)
    day_2 = models.ForeignKey(Day, related_name='day_2', null=True)
    day_3 = models.ForeignKey(Day, related_name='day_3', null=True)
    day_4 = models.ForeignKey(Day, related_name='day_4', null=True)
    day_5 = models.ForeignKey(Day, related_name='day_5', null=True)
    day_6 = models.ForeignKey(Day, related_name='day_6', null=True)
    day_7 = models.ForeignKey(Day, related_name='day_7', null=True)

    def __str__(self):
        return self.hostel


@python_2_unicode_compatible
class Feedback(models.Model):
    """
    Feedback model class.
    """
    full_name = models.CharField(max_length=100)
    webmail = models.CharField(
        max_length=50, help_text=_('Enter webmail without @iitg.ernet.in.'))
    mobile_number = models.CharField(max_length=12)
    mess_name = models.ForeignKey(Mess)
    meal_date = models.DateField()
    meal_type = models.CharField(max_length=20, choices=FOOD_TYPE)
    feedback_type = models.CharField(max_length=20, choices=FEEDBACK_TYPE)
    complaint_category = models.CharField(max_length=75, choices=COMPLAINT_TYPE)
    resolved = models.BooleanField(default=False, blank=True)
    complaint_detail = models.TextField(
        max_length=250, verbose_name='Details of Complaint')
    solution = models.TextField(
        max_length=250, verbose_name='Feasible/Proposed Solution')
    mmc_action = models.TextField(max_length=300, verbose_name='Action Taken',)
    creation_datetime = models.DateTimeField()
    last_updated = models.DateTimeField()

    def save(self, *args, **kwargs):
        if not self.id:
            self.creation_datetime = timezone.now()
        self.last_updated = timezone.now()
        return super(Feedback, self).save(*args, **kwargs)
