from django.shortcuts import redirect, render
from django.utils import timezone
from django.views.generic import FormView, View

from .forms import PublicFeedbackForm
from .models import Mess


class HomePage(View):
    http_method_names = ['get', 'head', 'options']
    template_name = 'mess_app/public/home.html'

    def get(self, request):
        # isoweekday() returns the day of the week as
        # an integer, where Monday is 1 and Sunday is 7.
        day = timezone.now().isoweekday()
        day = 1
        day_attr = 'day_' + str(day)
        mess_list = Mess.objects.all()
        mess_day = []
        for mess in mess_list:
            mess_day.append((mess.hostel, getattr(mess, day_attr)))
        args = dict(mess_list=mess_list, day_attr=day_attr, mess_day=mess_day)
        return render(request, self.template_name, args)


class MessDetail(View):
    http_method_names = ['get', 'head', 'options']
    template_name = 'mess_app/public/mess_detail.html'

    def get(self, request, hostel):
        mess_object = Mess.objects.get(hostel__iexact=hostel)
        day_attrs = ['day_' + str(i+1) for i in range(7)]
        days = [getattr(mess_object, day_attr) for day_attr in day_attrs]
        context = dict(days=days, hostel=hostel)
        return render(request, self.template_name, context)


class FeedbackCreate(View):
    template_name = 'mess_app/public/feedback_create.html'
    success_template = 'mess_app/public/feedback_confirm.html'

    def get(self, request):
        context = dict(form=PublicFeedbackForm())
        return render(request, self.template_name, context)

    def post(self, request):
        form = PublicFeedbackForm(request.POST)
        if form.is_valid():
            feedback = form.save()
            context = dict(feedback=feedback)
            return render(request, self.success_template, context)
        else:
            return render(request, self.template_name, dict(form=form))
