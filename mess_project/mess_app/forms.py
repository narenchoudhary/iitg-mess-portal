from django import forms
from django.utils.translation import ugettext_lazy as _

from .models import *


class LoginForm(forms.Form):
    """
    Genric login form.
    """
    username = forms.CharField(max_length=32)
    password = forms.CharField(widget=forms.PasswordInput)


class MealForm(forms.ModelForm):
    """
    ModelForm for updating Meal instances.
    """
    class Meta:
        model = Meal
        fields = ['menu_items', 'extra_items', 'special_dinner', 'price']
        widgets = {
            'menu_items': forms.Textarea(
                attrs={'class': 'materialize-textarea'}),
            'extra_items': forms.Textarea(
                attrs={'class': 'materialize-textarea'}),
            'special_dinner': forms.CheckboxInput(
                attrs={'class': 'filled-in'}
            )
        }


class PublicFeedbackForm(forms.ModelForm):
    """
    Modelform for taking public feedbacks.
    """
    class Meta:
        model = Feedback
        exclude = ['mmc_action', 'creation_datetime', 'last_updated', 'resolved']
        widgets = {
            'full_name': forms.TextInput(
                attrs={'placeholder': 'Narendra Choudhary'}
            ),
            'complaint_detail': forms.Textarea(
                attrs={
                    'class': 'materialize-textarea',
                    'placeholder': _('Add necessary details here '
                                     '(upto 250 characters allowed).')
                }),
            'solution': forms.Textarea(
                attrs={
                    'class': 'materialize-textarea',
                    'placeholder': _('Add details of proposed solution '
                                     'here (upto 250 characters allowed).')
                }),
            'meal_date': forms.DateInput(
                attrs={'class': 'datepicker'})
        }


class MMCFeedbackForm(forms.ModelForm):
    """
    Modelform for MMC users to update action taken on other user feedbacks.
    """
    class Meta:
        model = Feedback
        fields = ['resolved', 'mmc_action']

        widgets = {
            'mmc_action': forms.Textarea(
                attrs={
                    'class': 'materialize-textarea',
                    'placeholder': _('Add necessary details here '
                                     '(upto 300 characters allowed).')
                }
            ),
            'resolved': forms.CheckboxInput(
                attrs={'class': 'filled-in'}
            )
        }
