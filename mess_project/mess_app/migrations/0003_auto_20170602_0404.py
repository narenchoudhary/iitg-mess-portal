# -*- coding: utf-8 -*-
# Generated by Django 1.11.1 on 2017-06-02 04:04
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('mess_app', '0002_userprofile_hostel'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='mess',
            name='day_five',
        ),
        migrations.RemoveField(
            model_name='mess',
            name='day_four',
        ),
        migrations.RemoveField(
            model_name='mess',
            name='day_one',
        ),
        migrations.RemoveField(
            model_name='mess',
            name='day_seven',
        ),
        migrations.RemoveField(
            model_name='mess',
            name='day_six',
        ),
        migrations.RemoveField(
            model_name='mess',
            name='day_three',
        ),
        migrations.RemoveField(
            model_name='mess',
            name='day_two',
        ),
        migrations.AddField(
            model_name='mess',
            name='day_1',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='day_1', to='mess_app.Day'),
        ),
        migrations.AddField(
            model_name='mess',
            name='day_2',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='day_2', to='mess_app.Day'),
        ),
        migrations.AddField(
            model_name='mess',
            name='day_3',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='day_3', to='mess_app.Day'),
        ),
        migrations.AddField(
            model_name='mess',
            name='day_4',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='day_4', to='mess_app.Day'),
        ),
        migrations.AddField(
            model_name='mess',
            name='day_5',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='day_5', to='mess_app.Day'),
        ),
        migrations.AddField(
            model_name='mess',
            name='day_6',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='day_6', to='mess_app.Day'),
        ),
        migrations.AddField(
            model_name='mess',
            name='day_7',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='day_7', to='mess_app.Day'),
        ),
    ]
