from django import template
from django.template.loader import get_template

from mess_app.models import Mess

register = template.Library()


@register.simple_tag
def return_meal(mess, day, meal):
    day_object = getattr(mess, day)
    if day_object is not None:
        meal = getattr(day_object, meal)
        if meal is not None:
            return meal
    return "Not updated"


@register.inclusion_tag('hostels.html')
def hostel_list():
    hostels = Mess.objects.all().values('hostel')
    hostels = [h['hostel'] for h in hostels]
    return {'hostels': hostels}

