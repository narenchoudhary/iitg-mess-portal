from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from django.contrib.auth.forms import UserChangeForm, UserCreationForm

from .models import *


class UserProfileChangeForm(UserChangeForm):
    """
    Subclass of UserChangeForm used for form field in UserProfileAdmin
    """
    class Meta(UserChangeForm.Meta):
        model = UserProfile


class UserProfileCreationForm(UserCreationForm):
    """
    Subclass of UserCreationForm used for add_form field in UserProfileAdmin
    """
    class Meta(UserCreationForm.Meta):
        model = UserProfile


class UserProfileAdmin(UserAdmin):
    """
    Class that represents UserProfile model in the admin interface.
    """
    form = UserProfileChangeForm
    add_form = UserProfileCreationForm
    fieldsets = UserAdmin.fieldsets + (
        (None, {'fields': ('user_type', 'hostel')}),
    )
    list_display = ('username', 'user_type', 'is_active', 'hostel')
    list_filter = ('user_type', 'is_active', 'hostel')
    search_fields = ('username',)


admin.site.register(UserProfile, UserProfileAdmin)
admin.site.register(Meal)
admin.site.register(Day)
admin.site.register(Mess)
