from django.contrib import auth
from django.contrib.auth.mixins import LoginRequiredMixin, UserPassesTestMixin
from django.shortcuts import render, redirect
from django.urls import reverse_lazy
from django.utils.http import is_safe_url
from django.views.generic import ListView, TemplateView, UpdateView, View


from .forms import LoginForm, MealForm, MMCFeedbackForm
from .models import Day, Feedback, Mess


class LoginView(View):
    """
    Login View of MMC users.

    Render login form on GET, and authenticate and login
    on POST.
    """
    template_name = 'mess_app/login.html'
    next = ''

    def get(self, request):
        self.next = request.GET.get('next', '')
        if request.user.is_authenticated():
            if request.user.user_type == 'mmc':
                return redirect('mmc-home')
        args = dict(form=LoginForm(None), next=self.next)
        return render(request, self.template_name, args)

    def post(self, request):
        redirect_to = request.POST.get('next', self.next)
        form = LoginForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data.get('username')
            password = form.cleaned_data.get('password')

            user = auth.authenticate(username=username, password=password)

            if user is not None:
                if user.user_type == 'mmc':
                    auth.login(request=request, user=user)
                    if is_safe_url(redirect_to, request.get_host()):
                        return redirect(redirect_to)
                    else:
                        return redirect('mmc-home')
            else:
                form.add_error(None, 'No such user exists.')
                return render(request, self.template_name, dict(form=form))
        else:
            return render(request, self.template_name, dict(form=form))


class LogoutView(View):
    """
    Logout view for MMC users.
    """
    http_method_names = ['get', 'head', 'options']

    def get(self, request):
        auth.logout(request=request)
        return redirect('mmc-login')


class HomeView(UserPassesTestMixin, LoginRequiredMixin, TemplateView):
    """
    View class which handles rendering home page for MMC users.
    """
    template_name = 'mess_app/mmc/home.html'
    login_url = reverse_lazy('mmc-login')

    def test_func(self):
        return self.request.user.user_type == 'mmc'


class MessDetail(UserPassesTestMixin, LoginRequiredMixin, View):
    """
    View class which renders details of mess menu.
    """
    http_method_names = ['get', 'head', 'options']
    login_url = reverse_lazy('mmc-login')
    template_name = 'mess_app/mmc/mess_detail.html'

    def test_func(self):
        return self.request.user.user_type == 'mmc'

    def get_queryset(self, hostel):
        try:
            mess = Mess.objects.select_related(
                'day_1__breakfast', 'day_1__lunch', 'day_1__dinner',
                'day_2__breakfast', 'day_2__lunch', 'day_2__dinner',
                'day_3__breakfast', 'day_3__lunch', 'day_3__dinner',
                'day_4__breakfast', 'day_4__lunch', 'day_4__dinner',
                'day_5__breakfast', 'day_5__lunch', 'day_5__dinner',
                'day_6__breakfast', 'day_6__lunch', 'day_6__dinner',
                'day_7__breakfast', 'day_7__lunch', 'day_7__dinner',
            ).get(hostel=hostel)
        except Mess.DoesNotExist:
            mess = None
        return mess

    def get(self, request):
        days = ['day_1', 'day_2', 'day_3']
        meals = ['breakfast', 'lunch', 'dinner']
        mess = self.get_queryset(request.user.hostel)
        context = dict(mess=mess, days=days, meals=meals)
        return render(request, self.template_name, context)


class MealUpdate(UserPassesTestMixin, LoginRequiredMixin, View):
    """
    View class for updating a specific meal.

    GET:
    On GET, check if ``Meal`` instance exists and populate
    the ``MealForm`` if ``Meal`` instance exists.

    POST:
    In case``Mess`` instance corresponding to a hostel or
    a ``Day`` instance for a ``Mess`` or a ``Meal`` instance
    for a ``Day`` object doesn't exist at the time of POST
    request. Create required instances sequentially and
    save them.
    """
    login_url = reverse_lazy('mmc-login')
    template_name = 'mess_app/mmc/meal_update.html'

    meal_exists = False

    def test_func(self):
        return self.request.user.user_type == 'mmc'

    def get(self, request, day, meal):
        hostel = request.user.hostel
        print("Hostel is set to: {}".format(hostel))
        form = MealForm()
        try:
            # Check if Meal instance exists and initialize
            # the MealForm if instance exists.
            mess = Mess.objects.get(hostel=hostel)
            day_attr = 'day_' + day
            day_object = getattr(mess, day_attr)
            if day_object is not None:
                meal_object = getattr(day_object, meal)
                if meal_object is not None:
                    form = MealForm(instance=meal_object)
                    self.meal_exists = True
        except Mess.DoesNotExist:
                pass
        context = dict(day=day, meal=meal, form=form)
        return render(request, self.template_name, context)

    def post(self, request, day, meal):
        form = MealForm(request.POST)
        hostel = request.user.hostel
        if form.is_valid():
            try:
                # get Mess instance
                mess = Mess.objects.get(hostel=hostel)
            except Mess.DoesNotExist:
                # create Mess instance if it doesn't exist
                mess = Mess.objects.create(hostel=hostel)
            # Day attribute name Eg. day_1, day_2
            day_attr = 'day_' + day
            # get Day attribute from Mess instance
            day_object = getattr(mess, day_attr)
            # if Day instance doesn't exist, then create a
            # new Day instance
            if day_object is None:
                day_object = Day.objects.create(day=day)
                # Update the Mess instance with Day foreign key value
                setattr(mess, day_attr, day_object)
                mess.save()
            # Save Meal instance
            meal_object = form.save(commit=False)
            meal_object.food_type = meal
            meal_object.save()
            # Update Day instance with Meal foreign key value
            setattr(day_object, meal, meal_object)
            day_object.save()
            return redirect('mess-detail')
        else:
            context = dict(day=day, meal=meal, form=form)
            return render(request, self.template_name, context)


class FeedbackList(UserPassesTestMixin, LoginRequiredMixin, ListView):
    login_url = reverse_lazy('mmc-login')
    template_name = 'mess_app/mmc/feedback_list.html'
    context_object_name = 'feedback_list'

    def test_func(self):
        return self.request.user.user_type == 'mmc'

    def get_queryset(self):
        hostel = self.request.user.hostel
        return Feedback.objects.filter(mess_name__hostel__iexact=hostel)


class FeedbackUpdate(UserPassesTestMixin, LoginRequiredMixin, UpdateView):
    login_url = reverse_lazy('mmc-login')
    template_name = 'mess_app/mmc/feedback_update.html'
    form_class = MMCFeedbackForm
    model = Feedback
    success_url = reverse_lazy('mmc-feedback-list')

    def test_func(self):
        return self.request.user.user_type == 'mmc'
