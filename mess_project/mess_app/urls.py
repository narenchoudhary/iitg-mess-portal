from django.conf.urls import url

from . import views, views_mmc

urlpatterns = [
    url(r'^mmc/login/$', views_mmc.LoginView.as_view(), name='mmc-login'),
    url(r'^mmc/logout/', views_mmc.LogoutView.as_view(), name='mmc-logout'),
    url(r'^mmc/home/$', views_mmc.HomeView.as_view(), name='mmc-home'),
    url(r'^mmc/mess/detail/$', views_mmc.MessDetail.as_view(), name='mess-detail'),
    url(r'^mmc/meal/(?P<day>\d)/(?P<meal>[A-Za-z]+)/update/',
        views_mmc.MealUpdate.as_view(), name='meal-update'),
    url(r'^mmc/feedback/list/$', views_mmc.FeedbackList.as_view(),
        name='mmc-feedback-list'),
    url(r'^mmc/feedback/(?P<pk>\d+)/update/$', views_mmc.FeedbackUpdate.as_view(),
        name='mmc-feedback-update'),

    url(r'^public/home/$', views.HomePage.as_view(), name='public-home'),
    url(r'^public/mess/(?P<hostel>[a-zA-Z]+)/$', views.MessDetail.as_view(),
        name='public-mess-detail'),
    url(r'^public/feedback/create', views.FeedbackCreate.as_view(),
        name='public-feedback-create'),
]
