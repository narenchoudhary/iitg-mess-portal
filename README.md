# iitg-mess-portal

[![Project Status: WIP - Initial development is in progress, but there has not yet been a stable, usable release suitable for the public.](http://www.repostatus.org/badges/latest/wip.svg)](http://www.repostatus.org/#wip)

![logo](logo3.png)

Source code repository for general mess portal for IITG hostels.

Installation (for deveopment)
-----------------------------

    git clone https://github.com/narenchoudhary/iitg-mess-portal.git
    
Using **virtualenv** is recommended. Python3 was used to develop the portal. It should work fine with Python2.7, 
but still Python3 is recommended.

    virtualenv --python=/usr/bin/python3 venv
    source venv/bin/activate    
    pip install -r requirements/dev.txt
